from minty import Base


class TestMintyBaseClass:
    def test_base_normal_class_name(self):
        class NormalClass(Base):
            def __init__(self):
                pass

            def statsd_test(self):
                return self.statsd

        normal_class = NormalClass()
        s = normal_class.statsd_test()
        assert s.name == "NormalClass"

    def test_base_private_class_name(self):
        class _PrivateClass(Base):
            def __init__(self):
                pass

            def statsd_test(self):
                return self.statsd

        normal_class = _PrivateClass()
        s = normal_class.statsd_test()
        assert s.name == "PrivateClass"
