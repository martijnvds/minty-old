import pytest

from minty.exceptions import ConfigurationConflict
from minty.infrastructure.openstack_swift import (
    SwiftInfrastructure,
    SwiftWrapper,
)


class TestOpenStackSwiftInfrastructure:
    """Test the OpenStack/Swift infrastructure"""

    def setup(self):
        infra = SwiftInfrastructure()
        self.infra = infra

    def test_container_exception_swift_infra(self):
        faulty_config = {
            "no_storage_bucket_name_specified": {
                "name": "store name",
                "instance_uuid113424": "test",
            },
            "unsupported_container_key": {
                "storage_bucket11": "test-container-name"
            },
        }
        for test_case, config in faulty_config.items():
            with pytest.raises(ConfigurationConflict) as error:
                self.infra(config)
                assert (
                    error.value.message
                    == "No container name specified for Swift configuration"
                )

    def test_swift_infra_call(self):
        container_config = {
            "first_test_case": {
                "storage_bucket": "test-container-name",
                "filestore": {"name": "store name", "auth_version": "v3"},
            },
            "second_test_case": {
                "instance_uuid": "test",
                "filestore": [
                    {"name": "store name", "auth_version": "v3"},
                    {"name": "store name", "auth_version": "v3"},
                ],
            },
        }

        for test_case, config in container_config.items():
            swift_wrapper = self.infra(config=config)
            assert isinstance(swift_wrapper, SwiftWrapper)
            assert type(swift_wrapper.filestore_config) is list

    def test_swift_infra_call_with_no_filestore_config(self):
        container_config = {
            "first_test_case": {"storage_bucket": "test-container-name"},
            "second_test_case": {"instance_uuid": "test"},
        }
        for test_case, config in container_config.items():
            with pytest.raises(ConfigurationConflict) as error:
                self.infra(config)
                assert (
                    error.value.message
                    == "No file store configuration specified for openstack/swift"
                )
