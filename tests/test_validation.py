from pkgutil import get_data

import pytest

from minty.exceptions import ValidationError
from minty.validation import validate_with


class Dummy:
    @validate_with(get_data(__name__, "data/test_validation1.json"))
    def validation1(self, some_argument):
        return some_argument

    @validate_with(get_data(__name__, "data/test_validation2.json"))
    def validation2(
        self, some_argument=None, required_argument=None, object_argument={}
    ):
        return (some_argument, required_argument, object_argument)

    @validate_with(get_data(__name__, "data/test_validation3.json"))
    def validation3(self, required1, required2, required3):
        return (required1, required2, required3)


class TestValidation:
    def test_validation(self):
        d = Dummy()
        rv = d.validation1(some_argument="something")
        assert rv == "something"

        with pytest.raises(ValidationError) as error:
            d.validation1("something")

        error_object, = error.value.args
        assert error_object == [
            {
                "context": "required",
                "message": "'some_argument' is a required property",
                "cause": None,
                "property": "",
            }
        ]

    def test_validation_longer(self):
        d = Dummy()
        rv = d.validation2(required_argument="2019-05-20")
        assert rv == (None, "2019-05-20", {})

        with pytest.raises(ValidationError) as error:
            d.validation2(
                some_argument=1, object_argument={"sub_argument": "number"}
            )

        with pytest.raises(ValidationError) as error:
            d.validation2(
                some_argument="something",
                object_argument={"sub_argument": "number"},
            )

        error_object, = error.value.args
        assert error_object == [
            {
                "context": "format",
                "message": "'something' is not a 'uuid'",
                "cause": "badly formed hexadecimal UUID string",
                "property": "/some_argument",
            },
            {
                "context": "type",
                "message": "'number' is not of type 'number'",
                "cause": None,
                "property": "/object_argument/sub_argument",
            },
            {
                "context": "required",
                "message": "'required_sub_argument' is a required property",
                "cause": None,
                "property": "/object_argument",
            },
            {
                "context": "required",
                "message": "'required_argument' is a required property",
                "cause": None,
                "property": "",
            },
        ]

    def test_validation_strange(self):
        d = Dummy()
        rv = d.validation3(required1="a", required2="b", required3="c")
        assert rv == ("a", "b", "c")

        with pytest.raises(ValidationError) as error:
            d.validation3(required2="b")

        error_object, = error.value.args
        assert error_object == [
            {
                "context": "required",
                "message": "'required1' is a required property",
                "cause": None,
                "property": "",
            },
            {
                "context": "required",
                "message": "'required3' is a required property",
                "cause": None,
                "property": "",
            },
        ]

        with pytest.raises(ValidationError) as error2:
            d.validation3(required1="a", required2="b")

        error_object, = error2.value.args
        assert error_object == [
            {
                "context": "required",
                "message": "'required3' is a required property",
                "cause": None,
                "property": "",
            }
        ]
