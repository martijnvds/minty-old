import pytest

from minty.config.parser import ApacheConfigParser
from minty.config.store import RedisStore
from minty.config.store.base import ConfigurationNotFound


class TestRedisRetrieving:
    def test_get_configuration(self):
        store = RedisStore(
            parser=ApacheConfigParser(),
            arguments={
                "saas:instance:test.example.com": b"""
                    <instance>
                        Example = true
                    </instance>"""
            },
        )
        config = store.retrieve("test.example.com")
        assert config, "Redis did not return configuration"
        assert "Example" in config.keys(), "Config did not match 'Example'"

    def test_get_configuration_miss(self):
        store = RedisStore(
            parser=ApacheConfigParser(),
            arguments={
                "saas:instance:test.example.com": b"""
                    <instance>
                        Example = true
                    </instance>"""
            },
        )

        with pytest.raises(ConfigurationNotFound):
            store.retrieve("does-not-exist.example.com")
