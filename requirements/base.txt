## Code style tools
black
flake8
isort==4.3.10

## QA (tests, documentation)
sphinx
sphinx_rtd_theme
sphinx-autodoc-typehints
docutils==0.14

## Release
bumpversion

## Framework
amqpstorm
apacheconfig
file-magic
holidays
jsonpointer
jsonschema
python-keystoneclient
python-statsd
python-swiftclient
redis
boto3

