### Stage 1: Setup base python with requirements
FROM python:3.7-slim-stretch AS base-layer

COPY requirements/base.txt /tmp

# Add necessary packages to system
RUN apt-get update && \
    apt-get install -y git libmagic1 gcc && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get clean && \
    cd /tmp && \
    pip install -r base.txt && \
    apt-get autoremove -y --purge gcc

# Temp location: make sure we cache this bitch
RUN pip install ipython

# Setup application
COPY . /opt/minty
WORKDIR /opt/minty

FROM base-layer AS production

ENV OTAP=production

ENV PYTHONUNBUFFERED=on

FROM base-layer AS quality-and-testing

WORKDIR /tmp
COPY requirements/test.txt /tmp

RUN pip install -r test.txt

WORKDIR /opt/minty

RUN bin/generate_documentation.sh

FROM quality-and-testing AS development

ENV OTAP=development
